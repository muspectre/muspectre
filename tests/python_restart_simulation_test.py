#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   python_restart_simulation_test.py

@author Richard Leute <richard.leute@imtek.uni-freiburg.de>

@date   27 Jul 2022

@brief  Test if a problem run in one single simulation and a simulation
        restarted ones in between give the same results (stress, strain,
        all internal variables)

Copyright © 2022 Till Junge

µSpectre is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µSpectre is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with µSpectre; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""


import unittest
import time
import os
import errno
import numpy as np
from mpi4py import MPI

from python_test_imports import muFFT, muGrid
from python_test_imports import muSpectre as msp
ISI = msp.solvers.IsStrainInitialised


class RestartHelper(unittest.TestCase):
    """
    Helper class with functions for simulation start and evaluation
    """

    def __init__(self, nb_grid_pts, lengths, formulation, gradient, young,
                 poisson, yield_crit, hardening, cg_tol, newton_tol, equil_tol,
                 maxiter, verbose):
        self.nb_grid_pts = nb_grid_pts
        self.lens = lengths
        self.form = formulation
        self.gradient = gradient
        self.E = young
        self.nu = poisson
        self.yield_crit = yield_crit
        self.hardening = hardening

        self.cg_tol = cg_tol
        self.newton_tol = newton_tol
        self.equil_tol = equil_tol
        self.maxiter = maxiter
        self.verbose = verbose

    def save_delete_file(self, file_name_list, comm):
        if comm.rank == 0:
            for f_name in file_name_list:
                try:
                    os.remove(f_name)
                except OSError as e:
                    if e.errno != errno.ENOENT:
                        raise
        comm.barrier()

        return 0

    def read_results_from_netcdf_file(self, netcdf_filename, comm):
        cell, mat, _ = self.setup_simulation(comm, "serial")
        f = self.init_NetCDFFileIO(netcdf_filename,
                                   muGrid.FileIONetCDF.OpenMode.Read,
                                   comm=comm, cell=cell, mats=[mat])
        f.read(-1)
        result_dic = self.make_result_dic(cell=cell, mats=[mat])
        f.close()

        return result_dic

    def make_result_dic(self, cell, mats):
        result_dic = {"strain": cell.strain.array(),
                      "stress": cell.stress.array()}

        for mat in mats:
            mat_FC = mat.collection

            for k in mat_FC.keys():
                data = np.copy(mat_FC.get_field(k).array())
                result_dic[k] = data

        return result_dic

    def compare_result_dics(self, ref_dic, restart_dic):
        # compare keys
        self.assertTrue(ref_dic.keys() == restart_dic.keys())

        all_keys = ref_dic.keys()

        # compare fields and other entries (not StateFields)
        field_and_other_keys = \
            [k for k in all_keys if "sub_field index" not in k]
        for k in field_and_other_keys:
            compare_result = np.allclose(ref_dic[k], restart_dic[k])
            if not compare_result:
                print(f"not equal in key: '{k}'")

            self.assertTrue(compare_result)

        # Compare StateFields, they might be in a different order because of
        # the restart (here the actual StateField is always saved in index 0)
        # Therefore, we check all cyclic permutions of the StateFields
        state_field_keys = [k for k in all_keys if "sub_field index" in k]
        state_field_unique_prefix = \
            [up.split(",")[0] for up in state_field_keys]

        for up in state_field_unique_prefix:
            state_field_up_keys = [sf for sf in state_field_keys if up in sf]
            state_field_ref = []
            state_field_restart = []
            for sf in state_field_up_keys:
                state_field_ref.append(ref_dic[sf])
                state_field_restart.append(restart_dic[sf])

            state_field_ref = np.array(state_field_ref)
            state_field_restart = np.array(state_field_restart)

            # compare of the StatFields match in any cyclic permutation
            nb_history = state_field_ref.shape[0]
            compare = []
            for permut in range(nb_history):
                compare.append(np.allclose(state_field_ref,
                                           np.roll(state_field_restart,
                                                   permut, axis=0)))
            compare_res = any(compare)
            if not compare_res:
                print(f"The StateFields with unique prefix {up} differ!")

            self.assertTrue(compare_res)

    def setup_simulation(self, comm, fft):
        cell = msp.Cell(self.nb_grid_pts, self.lens,
                        self.form, self.gradient, fft, comm)
        mat = msp.material.MaterialHyperElastoPlastic1_3d.make(
            cell, "hpl", self.E, self.nu, self.yield_crit, self.hardening)
        for i, pixel in cell.pixels.enumerate():
            mat.add_pixel(i)

        solver = msp.solvers.KrylovSolverCG(cell, self.cg_tol,
                                            self.maxiter, self.verbose)
        cell.initialise()

        return cell, mat, solver

    def register_all_NetCDF_data(self, NetCDFFileIO_object, cell, mats):
        f = NetCDFFileIO_object

        f.register_field_collection(cell.get_field_collection(),
                                    ["REGISTER_ALL_FIELDS"],
                                    ["REGISTER_ALL_STATE_FIELDS"])
        for mat in mats:
            f.register_field_collection(mat.collection,
                                        ["REGISTER_ALL_FIELDS"],
                                        ["REGISTER_ALL_STATE_FIELDS"])

        return f

    def init_NetCDFFileIO(self, f_name, open_mode, comm, cell, mats):
        f = muGrid.FileIONetCDF(file_name=f_name,
                                open_mode=open_mode,
                                communicator=comm)
        f = self.register_all_NetCDF_data(f, cell, mats)

        return f

    def run_simulation(self, nc_file_name, DelF, nb_steps, comm, fft):
        # remove npy file if it already exists
        if comm.rank == 0:
            try:
                os.remove(nc_file_name)
            except OSError:
                pass
        comm.barrier()

        cell, mat, solver = self.setup_simulation(comm, fft)

        f = self.init_NetCDFFileIO(
            f_name=nc_file_name, open_mode=muGrid.FileIONetCDF.OpenMode.Write,
            comm=comm, cell=cell, mats=[mat])

        for i in range(nb_steps):
            msp.solvers.newton_cg(
                cell, DelF, solver, self.newton_tol, self.equil_tol,
                verbose=self.verbose,
                IsStrainInitialised=ISI.No if i == 0 else ISI.Yes)

            if i == nb_steps - 1:
                # write NetCDF file
                f.append_frame().write()

        f.close()

        return 0

    def restart_simulation(self, restart_nc_file_name, restart_frame,
                           DelF, nb_steps, comm, fft):
        cell, mat, solver = self.setup_simulation(comm, fft)

        f = self.init_NetCDFFileIO(
            f_name=restart_nc_file_name,
            open_mode=muGrid.FileIONetCDF.OpenMode.Append,
            comm=comm, cell=cell, mats=[mat])

        # set all parameters to the values stored in restart frame
        f.read(restart_frame)  # read in all fields

        for i in range(nb_steps):
            msp.solvers.newton_cg(
                cell, DelF, solver, self.newton_tol, self.equil_tol,
                verbose=self.verbose,
                IsStrainInitialised=ISI.Yes)

            if i == nb_steps - 1:
                # write NetCDF file
                f.append_frame().write()

        f.close()

        return 0


class SimulationRestart_Check(unittest.TestCase):
    def setUp(self):
        # set timing = True for timing information
        self.timing = False
        self.startTime = time.time()

        # clean up netcdf files, if True all fiels are deleted after testing
        self.cleanup = True

        # communicator
        self.comm = muGrid.Communicator(MPI.COMM_WORLD)

        # material/simulation parameters
        self.lens = [7.2, 6, 3]
        self.nb_grid_pts = [2, 2, 2]
        self.dim = len(self.nb_grid_pts)

        self.gradient = muFFT.Stencils3D.linear_finite_elements_5
        self.nb_quad_pts = int(len(self.gradient) / self.dim)

        self.fft = "serial"
        self.form = msp.Formulation.finite_strain

        self.E = 1  # Youngs modulus
        self.nu = 0.33  # Poisson ratio
        self.hardening = 1e-2 * self.E  # Hardening modulus
        self.yield_crit = 1e-3 * self.E  # yield stress

        self.newton_tol = 1e-6
        self.cg_tol = 1e-6
        self.equil_tol = 1e-6
        self.maxiter = 200
        self.verbose = msp.Verbosity.Silent

        self.sxy = -1e-4
        self.s_z = 1/((1+self.sxy)**2) - 1
        self.DelF = np.array([[self.sxy, 0, 0],
                              [0, self.sxy, 0],
                              [0, 0, self.s_z]])
        self.nb_steps_tot = 20

        self.nb_steps_1 = self.nb_steps_tot - 3
        self.nb_steps_2 = self.nb_steps_tot - self.nb_steps_1

        self.RH = RestartHelper(nb_grid_pts=self.nb_grid_pts,
                                lengths=self.lens,
                                formulation=self.form,
                                gradient=self.gradient,
                                young=self.E,
                                poisson=self.nu,
                                yield_crit=self.yield_crit,
                                hardening=self.hardening,
                                cg_tol=self.cg_tol,
                                newton_tol=self.newton_tol,
                                equil_tol=self.equil_tol,
                                maxiter=self.maxiter,
                                verbose=self.verbose)

        # file names
        self.test_file_name = "call_cell_restart.nc"

        self.ref_file_name = "restart_test_ref.nc"
        self.restart_file_name = "restart_test.nc"

        self.ref_file_name_cell = "restart_test_cell_ref.nc"
        self.restart_file_name_cell = "restart_test_cell.nc"

    def tearDown(self):
        if self.timing:
            t = time.time() - self.startTime
            print("{}:\n{:.3f} seconds".format(self.id(), t))

    def test_call_cell_write_read_restart_function(self):
        # ------------ clean up ------------- #
        try:
            os.remove(self.test_file_name)
        except FileNotFoundError:
            pass

        # just call the write function without any further testing
        cell = msp.Cell(self.nb_grid_pts, self.lens,
                        self.form, self.gradient, self.fft, self.comm)
        mat = msp.material.MaterialHyperElastoPlastic1_3d.make(
            cell, "hpl", self.E, self.nu, self.yield_crit, self.hardening)
        for i, pixel in cell.pixels.enumerate():
            mat.add_pixel(i)
        cell.initialise()

        cell.write_netcdf_restart_file(
            restart_file_name=self.test_file_name,
            open_mode=muGrid.FileIONetCDF.OpenMode.Write,
            frame=0,
            nb_simulation_step=0,
            communicator=self.comm)

        # just call the read function without any further testing
        cell = msp.Cell(self.nb_grid_pts, self.lens,
                        self.form, self.gradient, self.fft, self.comm)
        mat = msp.material.MaterialHyperElastoPlastic1_3d.make(
            cell, "hpl", self.E, self.nu, self.yield_crit, self.hardening)
        for i, pixel in cell.pixels.enumerate():
            mat.add_pixel(i)
        cell.initialise()

        cell.read_netcdf_restart_file(self.test_file_name, self.comm)

        # ------------ clean up ------------- #
        try:
            os.remove(self.test_file_name)
        except FileNotFoundError:
            pass

    def test_restart_manual(self):
        # --------- reference run --------- #
        self.RH.run_simulation(nc_file_name=self.ref_file_name,
                               DelF=self.DelF,
                               nb_steps=self.nb_steps_tot,
                               comm=self.comm, fft=self.fft)

        # --------- restart run 1. --------- #
        self.RH.run_simulation(nc_file_name=self.restart_file_name,
                               DelF=self.DelF,
                               nb_steps=self.nb_steps_1,
                               comm=self.comm, fft=self.fft)

        # --------- restart run 2. --------- #
        # read in restart file, restart and run last nb_steps = nb_steps_2
        # init mat cell and solver
        self.RH.restart_simulation(restart_nc_file_name=self.restart_file_name,
                                   restart_frame=-1,
                                   DelF=self.DelF, nb_steps=self.nb_steps_2,
                                   comm=self.comm, fft=self.fft)

        # --------- compare results --------- #
        # load reference run NetCDF file
        ref_dic = self.RH.read_results_from_netcdf_file(
            netcdf_filename=self.ref_file_name, comm=self.comm)

        # load restart run NetCDF file
        restart_dic = self.RH.read_results_from_netcdf_file(
            netcdf_filename=self.restart_file_name, comm=self.comm)

        # compare dicts
        self.RH.compare_result_dics(ref_dic=ref_dic,
                                    restart_dic=restart_dic)

        # ------------ clean up ------------- #
        if self.cleanup:
            os.remove(self.ref_file_name)
            os.remove(self.restart_file_name)

    def test_cell_restart_function(self):
        # delete all nc file because they might exist if the test previously
        # exited unexpected
        self.RH.save_delete_file([self.restart_file_name_cell,
                                  self.ref_file_name_cell],
                                 comm=self.comm)

        # --------- reference run --------- #
        self.RH.run_simulation(nc_file_name=self.ref_file_name_cell,
                               DelF=self.DelF,
                               nb_steps=self.nb_steps_tot,
                               comm=self.comm, fft=self.fft)

        # --------- restart run 1. --------- #
        cell_1, _, solver_1 = self.RH.setup_simulation(comm=self.comm,
                                                       fft=self.fft)
        # run simulation
        for i in range(self.nb_steps_1):
            msp.solvers.newton_cg(
                cell_1, self.DelF, solver_1, self.newton_tol, self.equil_tol,
                verbose=self.verbose,
                IsStrainInitialised=ISI.No if i == 0 else ISI.Yes)

        # write restart file
        cell_1.write_netcdf_restart_file(
            self.restart_file_name_cell,
            open_mode=muGrid.FileIONetCDF.OpenMode.Write,
            frame=0, nb_simulation_step=self.nb_steps_1,
            communicator=self.comm)

        # check if the value for 'nb_simulation_step' is correct
        nb_sim_step = cell_1.read_netcdf_restart_file_nb_simulation_step(
                self.restart_file_name_cell, self.comm)
        self.assertTrue(nb_sim_step == self.nb_steps_1)
        # check if the global parameter 'restart_frame' is wrote and read
        # correct
        restart_frame = cell_1.read_netcdf_restart_file_restart_frame(
            self.restart_file_name_cell, self.comm)
        self.assertTrue(restart_frame == 0)

        # --------- restart run 2. --------- #
        # read in restart file, restart and run last nb_steps = nb_steps_2
        # init mat cell and solver
        cell_2, _, solver_2 = self.RH.setup_simulation(comm=self.comm,
                                                       fft=self.fft)
        # read restart
        cell_2.read_netcdf_restart_file(self.restart_file_name_cell, self.comm)
        nb_done_steps = cell_2.read_netcdf_restart_file_nb_simulation_step(
            self.restart_file_name_cell, self.comm)
        self.assertTrue(nb_done_steps == self.nb_steps_1)

        # run simulation
        for i in range(self.nb_steps_tot - nb_done_steps):
            msp.solvers.newton_cg(
                cell_2, self.DelF, solver_2, self.newton_tol,
                self.equil_tol, verbose=self.verbose,
                IsStrainInitialised=ISI.Yes)  # It is necessary to set here
                                              # IsStrainInitialised=ISI.Yes
                                              # otherwise the strain will be
                                              # reinitialised wrong (to one/
                                              # zero for finite/small strain)

        # write all data to a NetCDF file for comparison (i.e. a restart file)
        cell_2.write_netcdf_restart_file(
            self.restart_file_name_cell,
            open_mode=muGrid.FileIONetCDF.OpenMode.Append, frame=1,
            nb_simulation_step=self.nb_steps_tot, communicator=self.comm)

        # check if the value for 'nb_simulation_step' is correct
        nb_sim_step = cell_1.read_netcdf_restart_file_nb_simulation_step(
            self.restart_file_name_cell, self.comm)
        self.assertTrue(nb_sim_step == self.nb_steps_tot)
        # check if the global parameter 'restart_frame' is wrote and read
        # correct
        restart_frame = cell_1.read_netcdf_restart_file_restart_frame(
            self.restart_file_name_cell, self.comm)
        self.assertTrue(restart_frame == 1)

        # --------- compare results --------- #
        # load reference run NetCDF file
        ref_dic = self.RH.read_results_from_netcdf_file(
            netcdf_filename=self.ref_file_name_cell, comm=self.comm)

        # load restart run NetCDF file
        restart_dic = self.RH.read_results_from_netcdf_file(
            netcdf_filename=self.restart_file_name_cell, comm=self.comm)

        # compare dicts
        self.RH.compare_result_dics(ref_dic=ref_dic,
                                    restart_dic=restart_dic)

        # ------------ clean up ------------- #
        if self.cleanup:
            os.remove(self.ref_file_name_cell)
            os.remove(self.restart_file_name_cell)
