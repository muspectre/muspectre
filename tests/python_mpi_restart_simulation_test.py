#!/usr/bin/env python3
# -*- coding:utf-8 -*-
"""
@file   python_mpi_restart_simulation_test.py

@author Richard Leute <richard.leute@imtek.uni-freiburg.de>

@date   28 Jul 2022

@brief  Test if a problem run in one single simulation and a simulation
        restarted ones in between give the same results (stress, strain,
        all internal variables)

Copyright © 2022 Till Junge

µSpectre is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public License as
published by the Free Software Foundation, either version 3, or (at
your option) any later version.

µSpectre is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
Lesser General Public License for more details.

You should have received a copy of the GNU Lesser General Public License
along with µSpectre; see the file COPYING. If not, write to the
Free Software Foundation, Inc., 59 Temple Place - Suite 330,
Boston, MA 02111-1307, USA.

Additional permission under GNU GPL version 3 section 7

If you modify this Program, or any covered work, by linking or combining it
with proprietary FFT implementations or numerical libraries, containing parts
covered by the terms of those libraries' licenses, the licensors of this
Program grant you additional permission to convey the resulting work.
"""


import unittest
import time
import os
import numpy as np
from mpi4py import MPI

from python_test_imports import muFFT, muGrid
from python_test_imports import muSpectre as msp
ISI = msp.solvers.IsStrainInitialised

from python_restart_simulation_test import RestartHelper


class SimulationRestart_MPI_Check(unittest.TestCase):
    def setUp(self):
        # set timing = True for timing information
        self.timing = False
        self.startTime = time.time()

        # clean up netcdf files, if True all fiels are deleted after testing
        self.cleanup = True

        # communicator
        self.comm = muGrid.Communicator(MPI.COMM_WORLD)

        # material/simulation parameters
        self.lens = [7.2, 6, 3]
        self.nb_grid_pts = [2, 3, 5]
        self.dim = len(self.nb_grid_pts)

        self.gradient = muFFT.Stencils3D.linear_finite_elements_5
        self.nb_quad_pts = int(len(self.gradient) / self.dim)

        self.form = msp.Formulation.finite_strain

        self.E = 1  # Youngs modulus
        self.nu = 0.33  # Poisson ratio
        self.hardening = 1e-2 * self.E  # Hardening modulus
        self.yield_crit = 1e-3 * self.E  # yield stress

        self.newton_tol = 1e-6
        self.cg_tol = 1e-6
        self.equil_tol = 1e-6
        self.maxiter = 200
        self.verbose = msp.Verbosity.Silent

        self.sxy = -1e-4
        self.s_z = 1/((1+self.sxy)**2) - 1
        self.DelF = np.array([[self.sxy, 0, 0],
                              [0, self.sxy, 0],
                              [0, 0, self.s_z]])
        self.nb_steps_tot = 20

        self.nb_steps_1 = self.nb_steps_tot - 3
        self.nb_steps_2 = self.nb_steps_tot - self.nb_steps_1

        self.RH = RestartHelper(nb_grid_pts=self.nb_grid_pts,
                                lengths=self.lens,
                                formulation=self.form,
                                gradient=self.gradient,
                                young=self.E,
                                poisson=self.nu,
                                yield_crit=self.yield_crit,
                                hardening=self.hardening,
                                cg_tol=self.cg_tol,
                                newton_tol=self.newton_tol,
                                equil_tol=self.equil_tol,
                                maxiter=self.maxiter,
                                verbose=self.verbose)

        self.ref_file_name = "restart_mpi_test_ref.nc"
        self.restart_file_name = "restart_mpi_test.nc"

        self.ref_file_name_cell = "restart_mpi_test_cell_ref.nc"
        self.restart_file_name_cell = "restart_mpi_test_cell.nc"

    def tearDown(self):
        if self.timing:
            t = time.time() - self.startTime
            print("{}:\n{:.3f} seconds".format(self.id(), t))

    def restart_mpi_manual(self, nb_procs_r1, fft_r1, nb_procs_r2, fft_r2):
        # delete all nc files because they might exist if the test previously
        # exited unexpected
        self.RH.save_delete_file([self.restart_file_name, self.ref_file_name],
                                 comm=self.comm)

        # --------- single run --------- #
        self.RH.run_simulation(nc_file_name=self.ref_file_name,
                               DelF=self.DelF,
                               nb_steps=self.nb_steps_tot,
                               comm=self.comm, fft="mpi")

        # --------- restart run 1. --------- #
        comm_1 = MPI.COMM_WORLD
        comm_smaller_1 = comm_1.Split(color=comm_1.rank//nb_procs_r1)
        if comm_1.rank < nb_procs_r1:
            comm_restart_1 = muGrid.Communicator(comm_smaller_1)
            if comm_restart_1.rank == 0:
                print(f"\n\nrestart 1 comm size {comm_restart_1.size}")
            self.RH.run_simulation(nc_file_name=self.restart_file_name,
                                   DelF=self.DelF,
                                   nb_steps=self.nb_steps_1,
                                   comm=comm_restart_1, fft=fft_r1)
        comm_1.Barrier()

        # --------- restart run 2. --------- #
        comm_2 = MPI.COMM_WORLD
        comm_smaller_2 = comm_2.Split(color=comm_2.rank//nb_procs_r2)
        if comm_2.rank < nb_procs_r2:
            comm_restart_2 = muGrid.Communicator(comm_smaller_2)
            if comm_restart_2.rank == 0:
                print(f"restart 2 comm size {comm_restart_2.size}")
            self.RH.restart_simulation(
                restart_nc_file_name=self.restart_file_name, restart_frame=-1,
                DelF=self.DelF, nb_steps=self.nb_steps_2, comm=comm_restart_2,
                fft=fft_r2)
        comm_2.Barrier()

        # --------- compare results --------- #
        # create a new communicator with only one rank to make a effective
        # (cell with comm that has only one rank leads to a serial
        # computation, no mpi) serial computation.
        comm = MPI.COMM_WORLD
        comm_serial_mpi = comm.Split(color=comm.rank)
        if comm.rank == 0:
            # ------------------- serial evaluation ------------------- #
            comm_serial = muGrid.Communicator(comm_serial_mpi)

            # load reference run NetCDF file
            ref_dic = self.RH.read_results_from_netcdf_file(
                netcdf_filename=self.ref_file_name, comm=comm_serial)

            # load restart run NetCDF file
            restart_dic = self.RH.read_results_from_netcdf_file(
                netcdf_filename=self.restart_file_name, comm=comm_serial)

            # compare dicts
            self.RH.compare_result_dics(ref_dic=ref_dic,
                                        restart_dic=restart_dic)

        # ------------ clean up ------------- #
        if self.cleanup and self.comm.rank == 0:
            os.remove(self.ref_file_name)
            os.remove(self.restart_file_name)

    def cell_restart_function(self, nb_procs_r1, fft_r1, nb_procs_r2, fft_r2):
        # delete all nc file because they might exist if the test previously
        # exited unexpected
        self.RH.save_delete_file([self.restart_file_name_cell,
                                  self.ref_file_name_cell],
                                 comm=self.comm)

        # --------- single run --------- #
        self.RH.run_simulation(nc_file_name=self.ref_file_name_cell,
                               DelF=self.DelF,
                               nb_steps=self.nb_steps_tot,
                               comm=self.comm, fft="mpi")

        # --------- restart run 1. --------- #
        comm_1 = MPI.COMM_WORLD
        comm_smaller_1 = comm_1.Split(color=comm_1.rank//nb_procs_r1)
        if comm_1.rank < nb_procs_r1:
            comm_restart_1 = muGrid.Communicator(comm_smaller_1)
            if comm_restart_1.rank == 0:
                print(f"\n\nrestart 1 comm size {comm_restart_1.size}")

            cell_1, _, solver_1 = self.RH.setup_simulation(comm=comm_restart_1,
                                                           fft=fft_r1)
            # run simulation
            for i in range(self.nb_steps_1):
                msp.solvers.newton_cg(
                    cell_1, self.DelF, solver_1, self.newton_tol,
                    self.equil_tol, self.verbose,
                    IsStrainInitialised=ISI.No if i == 0 else ISI.Yes)

            # write restart file
            cell_1.write_netcdf_restart_file(
                self.restart_file_name_cell,
                open_mode=muGrid.FileIONetCDF.OpenMode.Write,
                frame=0,
                nb_simulation_step=self.nb_steps_1,
                communicator=comm_restart_1)

            # check if the global parameter 'restart_frame' is wrote and read
            # correct
            restart_frame = cell_1.read_netcdf_restart_file_restart_frame(
                self.restart_file_name_cell, comm_restart_1)
            self.assertTrue(restart_frame == 0)

        comm_1.Barrier()

        # --------- restart run 2. --------- #
        comm_2 = MPI.COMM_WORLD
        comm_smaller_2 = comm_2.Split(color=comm_2.rank//nb_procs_r2)
        if comm_2.rank < nb_procs_r2:
            comm_restart_2 = muGrid.Communicator(comm_smaller_2)
            if comm_restart_2.rank == 0:
                print(f"restart 2 comm size {comm_restart_2.size}")

            cell_2, _, solver_2 = self.RH.setup_simulation(comm=comm_restart_2,
                                                           fft=fft_r2)
            # read restart
            cell_2.read_netcdf_restart_file(self.restart_file_name_cell,
                                            comm_restart_2)
            nb_prev_steps = cell_2.read_netcdf_restart_file_nb_simulation_step(
                self.restart_file_name_cell, communicator=comm_restart_2)

            self.assertTrue(nb_prev_steps == self.nb_steps_1)

            # run simulation
            for i in range(self.nb_steps_2):
                msp.solvers.newton_cg(
                    cell_2, self.DelF, solver_2, self.newton_tol,
                    self.equil_tol, verbose=self.verbose,
                    IsStrainInitialised=ISI.Yes)  # It is necessary to set here
                                                  # IsStrainInitialised=ISI.Yes
                                                  # otherwise the strain will
                                                  # be reinitialised wrong to
                                                  # (one/zero for finite-/small
                                                  # -strain)

            # write all data to a NetCDF file to compare (i.e. a restart file)
            cell_2.write_netcdf_restart_file(
                self.restart_file_name_cell,
                open_mode=muGrid.FileIONetCDF.OpenMode.Append, frame=1,
                nb_simulation_step=nb_prev_steps + self.nb_steps_2,
                communicator=comm_restart_2)

            # check if the global parameter 'restart_frame' is wrote and read
            # correct
            restart_frame = cell_2.read_netcdf_restart_file_restart_frame(
                self.restart_file_name_cell, comm_restart_2)
            self.assertTrue(restart_frame == 1)

        comm_2.Barrier()

        # --------- compare results --------- #
        # create a new communicator with only one rank to make a effective
        # (cell with comm that has only one rank leads to a serial
        # computation, no mpi) serial computation.
        comm = MPI.COMM_WORLD
        comm_serial_mpi = comm.Split(color=comm.rank)
        if comm.rank == 0:
            # ------------------- serial evaluation ------------------- #
            comm_serial = muGrid.Communicator(comm_serial_mpi)

            # load reference run NetCDF file
            ref_dic = self.RH.read_results_from_netcdf_file(
                netcdf_filename=self.ref_file_name_cell, comm=comm_serial)

            # load restart run NetCDF file
            restart_dic = self.RH.read_results_from_netcdf_file(
                netcdf_filename=self.restart_file_name_cell, comm=comm_serial)

            # compare dicts
            self.RH.compare_result_dics(ref_dic=ref_dic,
                                        restart_dic=restart_dic)

        comm.Barrier()

        # ------------ clean up ------------- #
        if self.cleanup and self.comm.rank == 0:
            os.remove(self.ref_file_name_cell)
            os.remove(self.restart_file_name_cell)

    def test_restart_mpi_manual(self):
        nb_procs = MPI.COMM_WORLD.size
        if nb_procs == 1:
            self.restart_mpi_manual(nb_procs_r1=nb_procs, fft_r1="mpi",
                                    nb_procs_r2=nb_procs, fft_r2="mpi")
        elif nb_procs == 2:
            self.restart_mpi_manual(nb_procs_r1=nb_procs, fft_r1="fftwmpi",
                                    nb_procs_r2=nb_procs-1, fft_r2="mpi")
        elif nb_procs > 2:
            self.restart_mpi_manual(nb_procs_r1=nb_procs, fft_r1="fftwmpi",
                                    nb_procs_r2=nb_procs-1, fft_r2="fftwmpi")
            self.restart_mpi_manual(nb_procs_r1=nb_procs, fft_r1="fftwmpi",
                                    nb_procs_r2=nb_procs-1, fft_r2="pfft")
            self.restart_mpi_manual(nb_procs_r1=nb_procs, fft_r1="fftwmpi",
                                    nb_procs_r2=1, fft_r2="serial")
            self.restart_mpi_manual(nb_procs_r1=1, fft_r1="serial",
                                    nb_procs_r2=nb_procs, fft_r2="fftwmpi")

    def test_cell_restart_function_mpi(self):
        nb_procs = MPI.COMM_WORLD.size
        if nb_procs == 1:
            self.cell_restart_function(nb_procs_r1=nb_procs, fft_r1="mpi",
                                       nb_procs_r2=nb_procs, fft_r2="mpi")
        elif nb_procs == 2:
            self.cell_restart_function(nb_procs_r1=nb_procs, fft_r1="fftwmpi",
                                       nb_procs_r2=nb_procs-1, fft_r2="mpi")
        elif nb_procs > 2:
            self.cell_restart_function(nb_procs_r1=nb_procs, fft_r1="fftwmpi",
                                       nb_procs_r2=nb_procs-1, fft_r2="fftwmpi")
            self.cell_restart_function(nb_procs_r1=nb_procs, fft_r1="fftwmpi",
                                       nb_procs_r2=nb_procs-1, fft_r2="pfft")
            self.cell_restart_function(nb_procs_r1=nb_procs, fft_r1="fftwmpi",
                                       nb_procs_r2=1, fft_r2="serial")
            self.cell_restart_function(nb_procs_r1=1, fft_r1="serial",
                                       nb_procs_r2=nb_procs, fft_r2="fftwmpi")
